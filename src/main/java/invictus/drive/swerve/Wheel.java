package invictus.drive.swerve;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj.PIDController;
import frc.robot.RobotMap;
import invictus.control.Utilities;
import invictus.enums.WheelLocation;
import invictus.models.PidConstants;
import invictus.sensors.SwerveModuleEncoder;

/**
 * Describes a swerve module in 3593's swerve drive chassis
 */
public class Wheel {
    public SwerveModuleEncoder HeadingEncoder;
    public CANSparkMax DriveMotor;
    public CANSparkMax RotatorMotor;
    public PIDController RotatorPidController;

    public Wheel(WheelLocation location, int driveMotorChannel, int turnMotorChannel, int encoderChannel, double rotatorOffset, PidConstants pidConstants) {
        System.out.println("[SWERVE] Created new " + location.toString() + " swerve wheel");
        System.out.println("========== Drive Channel: " + driveMotorChannel);
        System.out.println("========== Rotator Channel: " + turnMotorChannel);
        System.out.println("========== Encoder Channel: " + encoderChannel);
        System.out.println("========== Encoder Offset: " + rotatorOffset);
        
        DriveMotor = new CANSparkMax(driveMotorChannel, CANSparkMaxLowLevel.MotorType.kBrushless);
        DriveMotor.clearFaults();
        DriveMotor.setIdleMode(IdleMode.kBrake);

        RotatorMotor = new CANSparkMax(turnMotorChannel, CANSparkMaxLowLevel.MotorType.kBrushless);
        RotatorMotor.clearFaults();
        RotatorMotor.setInverted(true);
        RotatorMotor.setIdleMode(IdleMode.kBrake);

        HeadingEncoder = new SwerveModuleEncoder(encoderChannel, rotatorOffset);

        RotatorPidController = new PIDController(pidConstants.kP, pidConstants.kI, pidConstants.kD, 0, HeadingEncoder, RotatorMotor, 0.02);
        RotatorPidController.setInputRange(-180, 180);
        RotatorPidController.setOutputRange(-1, 1);
        RotatorPidController.setPercentTolerance(2);
        RotatorPidController.setContinuous(true);
        RotatorPidController.setEnabled(true);
    }

    public void SetDriveInverted(boolean inverted) {
        DriveMotor.setInverted(inverted);
    }

    // Control
    
    public void StopAllMotors() {
        DriveMotor.stopMotor();
        RotatorMotor.stopMotor();
    }

    public void SetAngle(double angleSetpoint) {
        RotatorPidController.setSetpoint(angleSetpoint);
    }

    public void SetMagnitude(double magnitude) {
        double gainedSpeed = magnitude * RobotMap.SpeedGain;
        double finalSpeed = Utilities.Deadband(gainedSpeed, RobotMap.SpeedDeadband);
        DriveMotor.set(finalSpeed);   
    }

    public void SetWheel(double magnitude, double angle) {
        SetMagnitude(magnitude);
        if (magnitude > RobotMap.SpeedDeadband) {
            SetAngle(angle);
        }
    }

    // Telemetry

    /**
     * Get current robot-centric angle heading of this wheel
     * @return Robot-centric wheel heading 0-360°
     */
    public double GetCurrentHeading() { 
        return HeadingEncoder.GetRelativeAngle(); 
    }

    /**
     * Get temperature of the motors
     * @return Array[2] of motor temperatures [Drive, Rotator]
     */
    public double[] GetMotorTemps() {
        double[] temps = new double[] { DriveMotor.getMotorTemperature(), RotatorMotor.getMotorTemperature() };
        return temps;
    }

    /**
     * Get current draw of the motors
     * @return Array[2] of motor currents [Drive, Rotator]
     */
    public double[] GetMotorOutputCurrents() {
        double[] currents = new double[] { DriveMotor.getOutputCurrent(), RotatorMotor.getOutputCurrent() };
        return currents;
    }

    public short GetDriverStickyFaults() {
        return DriveMotor.getStickyFaults();
    }

    public short GetRotatorStickyFaults() {
        return RotatorMotor.getStickyFaults();
    }
}