package invictus.pid;

import invictus.models.PidConstants;

public class PidController {
    private double _lastError = 0,
        _updateSpeed = 0.2,
        _integral = 0,
        _derivative = 0;
    private PidConstants pid;

    public PidController(PidConstants constants, double updateSpeed) {
        pid = constants;
        _updateSpeed = updateSpeed;
    }

    public PidController(PidConstants constants) {
        pid = constants;
    }

    public double GetNewPidValue(double setpoint, double actualValue) {
        double error = setpoint - actualValue;
        _integral += (error * _updateSpeed); // Integral is increased by the error*time (which is .02 seconds using normal IterativeRobot)
        _derivative = (error - _lastError) / _updateSpeed;
        double calculatedValue = (pid.kP * error) + (pid.kI * _integral) + (pid.kD * _derivative);
        return calculatedValue;
    }
}