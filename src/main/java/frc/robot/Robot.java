package frc.robot;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import invictus.control.Utilities;
import invictus.control.XboxInput;
import invictus.drive.swerve.*;

public class Robot extends TimedRobot {
  private SwerveChassis drive;
  XboxInput driveController;
  AHRS newGyro;
  static boolean DebugEnabled = true;

  @Override
  public void robotInit() {
    try {
      // Create a new drive chassis
      drive = new SwerveChassis(RobotMap.SwerveMap);

      // Initialize NavX IMU
      newGyro = new AHRS(SerialPort.Port.kUSB);
    } catch (Exception e) {
      DriverStation.reportError(e.getMessage(), e.getStackTrace());
    }

    // Reset gyro to 0
    newGyro.reset();

    // Initialize xbox controller
    driveController = new XboxInput(0);
  }

  @Override
  public void robotPeriodic() { }

  @Override
  public void teleopInit() { 
    newGyro.reset();
  }

  @Override
  public void teleopPeriodic() {
    double gyroAngle = Utilities.ClampContinuousAngle(newGyro.getAngle());

    // Subtract trigger 1 from trigger 2 to get a full -1 - 1 range and add a 0.1 deadband
    double triggerAxis = Utilities.Deadband(driveController.getRawAxis(2) - driveController.getRawAxis(3), 0.1);
    
    // Field-centric swerve drive
    drive.SwerveDrive(gyroAngle, (triggerAxis * 0.75), driveController.getX(Hand.kLeft), driveController.getY(Hand.kLeft));

    // Reset gyro on right bumper press
    if (driveController.getRawButton(6)) {
      SendDebug("Resetting Gyro");
      newGyro.reset();
    }
  }

  // Helper method for System.out.println
  public static void SendDebug(String message) {
    if (!DebugEnabled) return;

    System.out.println(message);
  }

  @Override
  public void disabledInit() {
    newGyro.reset();
    drive.StopAllMotors();
  }
}
