package invictus.drive.swerve;

import java.util.Arrays;
import java.util.Collections;

import invictus.control.Utilities;
import invictus.enums.WheelLocation;

public class SwerveChassis {
    private Wheel frontRightWheel, frontLeftWheel, rearRightWheel, rearLeftWheel;
    private boolean rotationLocked = false;
    private double TrackWidth, Wheelbase;

    public SwerveChassis(SwerveMap map) throws Exception {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("[SWERVE] SwerVictus v1 starting...");
        frontRightWheel = new Wheel(
            WheelLocation.kFR,
            map.GetDriveMotorChannel(WheelLocation.kFR), 
            map.GetRotatorMotorChannel(WheelLocation.kFR), 
            map.GetEncoderChannel(WheelLocation.kFR), 
            map.GetEncoderOffset(WheelLocation.kFR), 
            map.RotatorPid);
        frontRightWheel.SetDriveInverted(true);

        frontLeftWheel = new Wheel(
            WheelLocation.kFL,
            map.GetDriveMotorChannel(WheelLocation.kFL), 
            map.GetRotatorMotorChannel(WheelLocation.kFL), 
            map.GetEncoderChannel(WheelLocation.kFL), 
            map.GetEncoderOffset(WheelLocation.kFL),  
            map.RotatorPid);
        frontLeftWheel.SetDriveInverted(false);

        rearRightWheel = new Wheel(
            WheelLocation.kRR,
            map.GetDriveMotorChannel(WheelLocation.kRR), 
            map.GetRotatorMotorChannel(WheelLocation.kRR), 
            map.GetEncoderChannel(WheelLocation.kRR), 
            map.GetEncoderOffset(WheelLocation.kRR),  
            map.RotatorPid);
        rearRightWheel.SetDriveInverted(true);

        rearLeftWheel = new Wheel(
            WheelLocation.kRL,
            map.GetDriveMotorChannel(WheelLocation.kRL), 
            map.GetRotatorMotorChannel(WheelLocation.kRL), 
            map.GetEncoderChannel(WheelLocation.kRL), 
            map.GetEncoderOffset(WheelLocation.kRL),  
            map.RotatorPid);
        rearRightWheel.SetDriveInverted(true);

        Wheelbase = map.WheelbaseL;
        TrackWidth = map.WheelbaseW;

        System.out.println("[SWERVE] SwerVictus v1 successfully started");
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public void SetWheelsForward() {
        // set the wheels to base forward positions
    }

    public void SetAllWheelAngles(double angle) {
        if (rotationLocked) return;
        
        frontRightWheel.SetAngle(angle);
        frontLeftWheel.SetAngle(angle);
        rearRightWheel.SetAngle(angle);
        rearLeftWheel.SetAngle(angle);
    }

    public void SetAllWheelSpeeds(double angle) {
        frontRightWheel.SetMagnitude(angle);
        frontLeftWheel.SetMagnitude(angle);
        rearRightWheel.SetMagnitude(angle);
        rearLeftWheel.SetMagnitude(angle);
    }

    public void LockRotation(boolean locked) {
        // Set rotation lock
        rotationLocked = locked;
    }

    public void ToggleRotationLock() {
        rotationLocked = !rotationLocked;
    }

    /**
     * Stop all motors in this SwerveChassis
     */
    public void StopAllMotors() {
        // Stop all motors
        frontRightWheel.StopAllMotors();
        frontLeftWheel.StopAllMotors();
        rearRightWheel.StopAllMotors();
        rearLeftWheel.StopAllMotors();
    }

    /**
     * Robot-centric drive with 2 strafe axes and 1 rotation axis
     * @param RCW
     * @param STR
     * @param FWD
     */
    public void SwerveDrive(double RCW, double STR, double FWD) {
        SwerveDrive(0, RCW, STR, FWD);
    }

    /**
     * Field-centric drive with 2 strafe axes and 1 rotation axis
     * @param gyroAngle 0-360 clockwise gyro angle
     * @param RCW -1 - 1 rotation coefficient
     * @param STR -1 - 1 X-axis strafe control
     * @param FWD -1 - 1 Y-axis strafe control
     */
    public void SwerveDrive(double gyroAngle, double RCW, double STR, double FWD) {
        FWD *= -1;

        // Adjust vectors for gyro angle
        double gyroAngleRadians = Math.toRadians(gyroAngle);
        double adjustedFWD = (FWD * Math.cos(gyroAngleRadians)) + (STR * Math.sin(gyroAngleRadians));
        STR = (-FWD * Math.sin(gyroAngleRadians)) + (STR * Math.cos(gyroAngleRadians));
        FWD = adjustedFWD;

        double wheelBase = Wheelbase;
        double trackWidth = TrackWidth;
        double radius = Math.sqrt(Math.pow(wheelBase, 2) + Math.pow(trackWidth, 2));

        double A = STR - RCW * (wheelBase / radius);
        double B = STR + RCW * (wheelBase / radius);
        double C = FWD - RCW * (trackWidth / radius);
        double D = FWD + RCW * (trackWidth / radius);

        // Front right
        double frontRightSpeed = Math.sqrt(Math.pow(B, 2) + Math.pow(C, 2));
        double frontRightAngle = Math.atan2(B, C) * 180/Math.PI;

        // Front left
        double frontLeftSpeed = Math.sqrt(Math.pow(B, 2) + Math.pow(D, 2));
        double frontLeftAngle = Math.atan2(B, D) * 180/Math.PI;

        // Rear right
        double rearRightSpeed = Math.sqrt(Math.pow(A, 2) + Math.pow(C, 2));
        double rearRightAngle = Math.atan2(A, C) * 180/Math.PI;

        // Rear left
        double rearLeftSpeed = Math.sqrt(Math.pow(A, 2) + Math.pow(D, 2));
        double rearLeftAngle = Math.atan2(A, D) * 180/Math.PI;

        // If, after calculating the 4 wheel speeds, any of them is greater than 1, then divide all the wheel speeds by the largest value.
        if (frontRightSpeed > 1 || frontLeftSpeed > 1 || rearRightSpeed > 1 || rearLeftSpeed > 1) {
            Double[] speeds = new Double[] { frontRightSpeed, frontLeftSpeed, rearRightSpeed, rearLeftSpeed };
            double maxValue = Collections.max(Arrays.asList(speeds));

            frontRightSpeed /= maxValue;
            frontLeftSpeed /= maxValue;
            rearRightSpeed /= maxValue;
            rearLeftSpeed /= maxValue;
        }

        // Calculated angles range from -180° to 180°
        frontRightAngle = Utilities.ClampAngle(frontRightAngle);
        frontLeftAngle = Utilities.ClampAngle(frontLeftAngle);
        rearRightAngle = Utilities.ClampAngle(rearRightAngle);
        rearLeftAngle = Utilities.ClampAngle(rearLeftAngle);

        frontRightWheel.SetWheel(frontRightSpeed, frontRightAngle);
        frontLeftWheel.SetWheel(frontLeftSpeed, frontLeftAngle);
        rearRightWheel.SetWheel(rearRightSpeed, rearRightAngle);
        rearLeftWheel.SetWheel(rearLeftSpeed, rearLeftAngle);
    }

    public void SteerDrive(double speed, double rotation) {
        // drive like a car: 1 speed axis and 1 rotation axis
        // No rotation for rear wheels
    }
}