package invictus.control;

import edu.wpi.first.wpilibj.XboxController;

public class XboxInput extends XboxController {
    
    public XboxInput(int port) {
        super(port);
    }

    public Joystick GetLeftJoystick() {
        return new Joystick(this.getRawAxis(0), this.getRawAxis(1));
    }

    public Joystick GetRightJoystick() {
        return new Joystick(this.getRawAxis(4), this.getRawAxis(5));
    }

    public double GetTriggerAxis() {
        return this.getRawAxis(2) - this.getRawAxis(3);
    }
}