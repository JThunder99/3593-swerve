package invictus.enums;

public enum WheelLocation {
    kFL,
    kFR,
    kRL,
    kRR
}