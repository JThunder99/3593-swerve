package frc.robot;

import invictus.drive.swerve.SwerveMap;
import invictus.models.PidConstants;

public final class RobotMap {
    // SwerveMap
    public static SwerveMap SwerveMap = new SwerveMap(
        10, 12, 15, 16,
        11, 13, 14, 17,
        0, 1, 2, 3,
        2900, 1618, 2247, 976,
        26.325, 13.325,
        new PidConstants(5e-3, 1e-10, 1e-8)
    );

    public static final double SpeedDeadband = 0.05;
    public static final double SpeedGain = 0.5;
}